var audioContext, analyser, audioUtil

const canvasSketch = require('canvas-sketch');
var createAnalyser = require('web-audio-analyser')
var random = require('canvas-sketch-util/random')
var palettes = require('nice-color-palettes')
const BezierEasing = require('bezier-easing')
var average = require('analyser-frequency-average')
const glslify = require('glslify')
global.THREE = require('three')
var OBJLoader = require('three-obj-loader')(THREE)
  , EffectComposer = require('three-effectcomposer')(THREE)
  , { RenderPass } = require('three-effectcomposer')(THREE)
  , { ShaderPass } = require('three-effectcomposer')(THREE)
  , { FilmPass } = require('three-effectcomposer')(THREE)
  , { CopyShader } = require('three-effectcomposer')(THREE)


var objLoader  = new THREE.OBJLoader();
var fontLoader = new THREE.FontLoader();

var threeAddons = require('three-addons')
var io = require('socket.io-client')
var axios = require('axios')

var socket = io('localhost:3000')

require('three/examples/js/controls/OrbitControls');

// require('three/examples/fonts')

const frag = glslify(/* glsl */`
  varying vec2 vUv;
  varying vec3 v_normal;
  varying vec3 bump_map;
  uniform vec3 color;
  uniform float time;
  uniform float high;
  uniform float mid;
  uniform float low;
  uniform sampler2D tAudioData;
  uniform float nData;

  #pragma glslify: snoise2 = require(glsl-noise/simplex/2d);
  #pragma glslify: noise = require('glsl-noise/simplex/3d');
  #pragma glslify: hsl2rgb = require('glsl-hsl2rgb');

  void main () {
    vec3 color_scale = vec3(low,mid,high);

    // float offset = 0.02 * noise(vec3(vUv.xy * 1., time * 2.4));

    gl_FragColor = vec4(color, 0.3);
  }
`);

const vert = glslify(/* glsl */`
  #pragma glslify: snoise4 = require(glsl-noise/simplex/4d);
  varying vec2 vUv;
  varying vec3 v_normal;
  varying vec3 bump_map;
  uniform float time;
  uniform float high;
  uniform float mid;
  uniform float low;
  uniform float noise;
  uniform float nData;
  uniform float displacement;

  void main () {
    v_normal = normal;

    vec3 pos = position.xyz;
    pos += snoise4(vec4(position.xyz, time * .1) * 10.) * 0.005;

    bump_map = pos;

    gl_Position = projectionMatrix * modelViewMatrix * vec4(pos, 1.0);
  }
`);

const palette = random.pick(palettes)

console.log("PALETTE > ", palette)
const settings = {
  animate: true,
  fps: 30,
  dimensions: [ 1920, 1080 ], 
  context: 'webgl',
  attributes: { antialias: true }
};

var lastFrame = {
  lastLow: 0.2,
  lastMid: 0.2,
  lastHigh: 0.2,
}

const loadFont = (path) => {
  // async
  return new Promise(resolve => {
    fontLoader.load(path, resolve)
  })
}

const addText = (text, options) => {

  return loadFont('fonts/helvetiker.js').then(font => {
    // not async
    var mesh
    textGeometery = new THREE.TextGeometry(
      text, 
      {
        font: font, 
        size: 0.2, 
        height: 0.01,
        curveSegments: 1,
      }
    )
    mesh = new THREE.Mesh(
      textGeometery,
      new THREE.MeshPhongMaterial({     
        color: random.pick(palette),
        flatShading: true,
      } )
      // new THREE.ShaderMaterial({
      //   fragmentShader: frag,
      //   vertexShader: vert,
      //   uniforms: { 
      //     color: { value: new THREE.Color(random.pick(palette)) },
      //     time: {  value: 0 },
      //     high: {  value: 0 },
      //     mid: {  value: 0 },
      //     low: {  value: 0 },
      //   }
      // })
    )
    mesh.life = options.life
    mesh.index = options.index
    return mesh
  })
}

const sketch = ({ context }) => {
  var renderTargetParameters = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBAFormat, stencilBufer: false };
  const renderTarget = new THREE.WebGLRenderTarget(context.drawingBufferHeight, context.drawingBufferWidth, renderTargetParameters)
  console.log(">>>>>>>>>>>>>>", renderTarget)

  const renderer = new THREE.WebGLRenderer({
    context
  });



  renderer.setClearColor(random.pick(palette), 1);
  // renderer.setClearColor('#000000', 1);
  const origin = new THREE.Vector3()

  let camera = new THREE.PerspectiveCamera(
    45, 45, 1, 1000
  )
  camera.position.set(0, 5, 5);
  camera.lookAt(origin);
  const controls = new THREE.OrbitControls(camera);

  const scene = new THREE.Scene();

  var planet = new THREE.Mesh(
    new THREE.SphereGeometry( 1, 6, 6 ),
    // new THREE.ShaderMaterial({
    //   fragmentShader: frag,
    //   vertexShader: vert,
    //   uniforms: { 
    //     color: { value: new THREE.Color(random.pick(palette)) },
    //     time: {  value: 0 },
    //     high: {  value: 0 },
    //     mid: {  value: 0 },
    //     low: {  value: 0 },
    //   }
    // })
    new THREE.MeshPhongMaterial({     
      color: random.pick(palette),
      flatShading: true
    } )
  )
  scene.add(planet)
   
  var vapsquadTitle, vapsquadSpaceship, text

  var chat = []
  
  addText('Loaded!', {index: chat.length}).then(mesh => {
    chat.push(mesh)
    scene.add(mesh)
  })

  objLoader.load('objects/voice_acting_power_squad_extruded.obj', event => {
    let scale = 0.5
    vapsquadTitle = event.children[0]
    vapsquadTitle.material.color.r = 0.1
    vapsquadTitle.scale.set(scale,scale,scale)
    vapsquadTitle.position.set(-3,-2.7,0)
    vapsquadTitle.phyiscs = {}
    scene.add(vapsquadTitle)
  })

  objLoader.load('objects/vapsquad-spaceship.obj', event => {
    let scale = 0.05
    
    vapsquadSpaceship = event.children[0]
    vapsquadSpaceship.scale.set(scale,scale,scale)
    vapsquadSpaceship.rotation.y = Math.PI * 0.75
    vapsquadSpaceship.scale.set(scale,scale,scale)
    vapsquadSpaceship.phyiscs = {
      state: 0,
      height: 0.2,
    }
    scene.add(vapsquadSpaceship)
  })
  
  scene.add(new THREE.AmbientLight("#ffffff", 0.3));

  const backLight = new THREE.PointLight(random.pick(palette), 2.5);
  backLight.position.set(10, 15, -5)
  backLight.lookAt(origin)
  scene.add(backLight);
  
  const frontLight = new THREE.PointLight("#ffffff", 1);
  frontLight.position.set(-5, -5, 5)
  frontLight.lookAt(origin)
  scene.add(frontLight);
  let half_pi = Math.PI * 0.5
  
  var gridHelperX = new THREE.GridHelper( 10, 10 );
  var gridHelperY = new THREE.GridHelper( 10, 10 );
  var gridHelperZ = new THREE.GridHelper( 10, 10 );
  gridHelperY.rotation.x = Math.PI * 0.5
  gridHelperZ.rotation.set(half_pi,0,Math.PI * 0.5)

  // scene.add( gridHelperX );
  // scene.add( gridHelperY );
  // scene.add( gridHelperZ );

  navigator.mediaDevices.getUserMedia( {audio:true})
  .then( stream => {
    audioContext = new AudioContext()
  
    var mediaStreamSource = audioContext.createMediaStreamSource(stream)
    mediaStreamSource.connect(audioContext.destination)

    audioUtil = createAnalyser(mediaStreamSource, audioContext, {mute: true}) 

    analyser = audioUtil.analyser
    analyser.fftSize = 256
    analyser.smoothingTimeConstant = 0.8
      // 0 = rage
      // 0.8 = fun
      // 2.2 = chill
  })

  let timer = 0
  let timerLimit = 60


  socket.on('chat', (data) =>{
    let options = {
      index: chat.length,
      life: 200,
    }
    axios.get(`https://jsonplaceholder.typicode.com/comments/${chat.length + 1}`)
    .then(res => {
      let message = `${data.context.username}: ${data.msg}`
      addText(message, options).then(mesh => {
        mesh.position.x = 2
        chat.push(mesh)
        scene.add(mesh)
      })
    })
    console.log("Chat Recieved:",data)
  });

  socket.connect()
  let easeFn = BezierEasing(.28, .85, .43, 1)
  // let easeFn = BezierEasing(.56,.66,.83,.67) // star wars
  
  // COMPOSER
  
  // const composer = new EffectComposer(renderer);
  // var renderPass = new RenderPass(scene, camera)
  // var copyPass = new ShaderPass( CopyShader )
  // composer.addPass(renderPass)
  // composer.addPass(copyPass)
  // copyPass.renderToScreen = true
  
  // composer.setRenderTarget( renderTarget )
  // const filmPass = new EffectComposer.FilmPass(
  //   0.35,   // noise intensity
  //   0.025,  // scanline intensity
  //   648,    // scanline count
  //   false,  // grayscale
  // );
  // composer.addPass(filmPass)

  document.title = "Vapsquad Overlay"
  console.log(">>>>>>>>>>>>>>>>>>", document.title)
  
  var then = 0
  return {
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
      
    },

    render ({ time }) {
      controls.update();
      if(!analyser || !vapsquadTitle) return
      let deltaTime = time - then
      then = time
      // Audio Analysis

      let low = average(analyser, audioUtil.frequencies(), 50, 150)
      let mid = average(analyser, audioUtil.frequencies(), 400, 2000)
      let high = average(analyser, audioUtil.frequencies(), 2500, 15000)
      
      low = (low > 0.2 ? (low + lastFrame.low) * 0.5 : 0.2) 
      mid = (mid > 0.2 ? (mid + lastFrame.mid) * 0.5 : 0.2) 
      high = (high > 0.2 ? (high + lastFrame.high) * 0.5 : 0.2) 
      
      frontLight.intensity = mid + 1
      backLight.intensity = high + 0.5

      lastFrame.low = low
      lastFrame.mid = mid
      lastFrame.high = high
      
      timer += 1
  
      let cos = Math.cos(time)
      let sin = Math.sin(time)

      // Update Objects State

      for (let i = 0; i < chat.length; i++) {
        const text = chat[i];
        
      // chat.forEach(text => {
        let y = Math.abs(text.index - chat.length)
        // text.position.y = y - 5
        text.position.x += 0.001
        text.position.z -= 0.005
        text.life *= 0.995
        text.opacity = text.life 
        text.scale.set(1,1,1).multiplyScalar(easeFn(text.life / 200))
        if(text.life < 0) {
          scene.remove(text)
          let sh = chat.splice(i,1)
        }
        // if shader material
        if (text.uniforms){
          text.material.uniforms.time.value = time
          text.material.uniforms.high.value = high
          text.material.uniforms.mid.value = mid
          text.material.uniforms.low.value = low
          
          if(text.index == chat.length - 1) {
          }
        }
      }

      // Update Positions and Rotation

      vapsquadTitle.scale.y = high * 10
      vapsquadTitle.position.y = -5 + high
      vapsquadTitle.position.z = 0
      vapsquadTitle.position.x = -6.5
      vapsquadTitle.material.color.r = low
      vapsquadTitle.material.color.g = mid
      vapsquadTitle.material.color.b = high
      vapsquadTitle.rotation.x = Math.PI * 0.25

      if(low / 3 > vapsquadSpaceship.phyiscs.height) vapsquadSpaceship.phyiscs.height = low / 3 
      let heightOffset = vapsquadSpaceship.phyiscs.height / 2
      
      // TODO: somehow add physics to objects.
      let topX = 3.1
      let topY = 1.9
      planet.rotation.y -= 0.005 //+ mid * 0.05 + (low - 0.2) * 0.02
      planet.position.x = topX
      planet.position.y = topY
      planet.scale.set(1,1,1).multiplyScalar(low * 0.1 + 0.2)

      vapsquadSpaceship.position.x = sin * (vapsquadSpaceship.phyiscs.height + heightOffset) + topX
      vapsquadSpaceship.position.z = cos * (vapsquadSpaceship.phyiscs.height + heightOffset) //+ topY
      vapsquadSpaceship.position.y = topY
      vapsquadSpaceship.phyiscs.state += (high - 0.2) + sin * 0.01
      vapsquadSpaceship.rotation.z = Math.sin(vapsquadSpaceship.phyiscs.state) * Math.PI * 0.2
      vapsquadSpaceship.rotation.y = time + half_pi
      
      if (vapsquadSpaceship.phyiscs.height > 0.2) vapsquadSpaceship.phyiscs.height -= 0.001

      // composer.render(0.1);
      renderer.render(scene, camera);
    },

    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
