var audioContext, analyser, audioUtil;

global.THREE = require("three");
const canvasSketch = require("canvas-sketch");
const createAnalyser = require("web-audio-analyser");
const random = require("canvas-sketch-util/random");
const palettes = require("nice-color-palettes");
const BezierEasing = require("bezier-easing");
const average = require("analyser-frequency-average");
const OBJLoader = require("three-obj-loader")(THREE);

const objLoader = new THREE.OBJLoader();
const fontLoader = new THREE.FontLoader();

const io = require("socket.io-client");
// const socket = io();

require("three/examples/js/controls/OrbitControls");

const palette = random.pick(palettes);

console.log("PALETTE > ", palette);
const settings = {
  animate: true,
  fps: 30,
  dimensions: [1920, 1080],
  context: "webgl",
  attributes: { antialias: true },
  margin: 0,
};

var lastFrame = {
  lastLow: 0.2,
  lastMid: 0.2,
  lastHigh: 0.2,
};

const loadFont = (path) => {
  // async
  return new Promise((resolve) => {
    fontLoader.load(path, resolve);
  });
};

const addActor = (payload, options) => {
  return loadFont("fonts/helvetiker.js").then((font) => {
    var mesh;
    textGeometery = new THREE.TextGeometry(payload.handle, {
      font: font,
      size: 0.15,
      height: 0.01,
      curveSegmments: 4,
    });
    // textGeometery.center()
    mesh = new THREE.Mesh(
      textGeometery,
      new THREE.MeshPhongMaterial({
        color: "#ffffff",
        // color: payload.color,
        flatShading: true,
      })
    );
    mesh.life = options.life + options.life * options.index;
    mesh.maxLife = mesh.life;
    mesh.index = options.index;
    return mesh;
  });
};

const addText = (text, options) => {
  return loadFont("fonts/helvetiker.js").then((font) => {
    var mesh;
    textGeometery = new THREE.TextGeometry(text, {
      font: font,
      size: 0.2,
      height: 0.01,
      curveSegments: 1,
    });
    textGeometery.center();
    mesh = new THREE.Mesh(
      textGeometery,
      new THREE.MeshPhongMaterial({
        color: "#cccccc", //random.pick(palette),
        flatShading: true,
      })
    );
    mesh.life = options.life;
    mesh.index = options.index;
    return mesh;
  });
};

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({
    context,
  });

  // renderer.setClearColor(random.pick(palette), 1);
  renderer.setClearColor("#000000", 0);
  const origin = new THREE.Vector3();

  const camera = new THREE.PerspectiveCamera(45, 45, 1, 1000);
  camera.position.set(0, 5, 5);
  camera.lookAt(origin);
  const controls = new THREE.OrbitControls(camera);

  const scene = new THREE.Scene();

  var planet = new THREE.Mesh(
    new THREE.SphereGeometry(1, 6, 6),
    new THREE.MeshPhongMaterial({
      color: random.pick(palette),
      flatShading: true,
    })
  );
  scene.add(planet);

  var vapsquadTitle, vapsquadSpaceship, text;

  var chat = [];
  var segment = [];
  var actors = [];
  var cast = [];

  objLoader.load("objects/voice_acting_power_squad_extruded.obj", (event) => {
    let scale = 0.5;
    vapsquadTitle = event.children[0];
    vapsquadTitle.material.color.r = 0.1;
    vapsquadTitle.scale.set(scale, scale, scale);
    vapsquadTitle.position.set(-3, -2.7, 0);

    vapsquadTitle.phyiscs = {};
    scene.add(vapsquadTitle);
  });

  objLoader.load("objects/vapsquad-spaceship.obj", (event) => {
    let scale = 0.05;
    vapsquadSpaceship = event.children[0];
    vapsquadSpaceship.scale.set(scale, scale, scale);
    vapsquadSpaceship.rotation.y = Math.PI * 0.75;
    vapsquadSpaceship.scale.set(scale, scale, scale);
    vapsquadSpaceship.phyiscs = {
      state: 0,
      height: 0.2,
    };
    scene.add(vapsquadSpaceship);
  });

  scene.add(new THREE.AmbientLight("#ffffff", 0.3));

  const backLight = new THREE.PointLight(random.pick(palette), 0.5);
  backLight.position.set(10, 15, -5);
  backLight.lookAt(origin);
  scene.add(backLight);

  const frontLight = new THREE.PointLight("#ffffff", 0.7);
  frontLight.position.set(-5, -5, 5);
  frontLight.lookAt(origin);
  scene.add(frontLight);

  const movingLight = new THREE.PointLight(random.pick(palette), 0.6);
  movingLight.position.set(0, 4, 2.5);
  movingLight.lookAt(origin);
  scene.add(movingLight);

  // var pointLightHelper = new THREE.PointLightHelper( movingLight, 1.2 );
  // scene.add( pointLightHelper );

  let half_pi = Math.PI * 0.5;
  var gridHelperX = new THREE.GridHelper(10, 10);
  var gridHelperY = new THREE.GridHelper(10, 10);
  var gridHelperZ = new THREE.GridHelper(10, 10);
  gridHelperY.rotation.x = Math.PI * 0.5;
  gridHelperZ.rotation.set(half_pi, 0, Math.PI * 0.5);

  // scene.add( gridHelperX );
  // scene.add( gridHelperY );
  // scene.add( gridHelperZ );

  navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
    audioContext = new AudioContext();

    var mediaStreamSource = audioContext.createMediaStreamSource(stream);
    mediaStreamSource.connect(audioContext.destination);

    audioUtil = createAnalyser(mediaStreamSource, audioContext, { mute: true });

    analyser = audioUtil.analyser;
    analyser.fftSize = 256;
    analyser.smoothingTimeConstant = 1.8;
    // 0 = rage
    // 0.8 = fun
    // 2.2 = chill
  });

  let timer = 0;
  let timerLimit = 60;

  updateBooth = (data) => {
    console.log(data);
    actors.forEach((mesh) => scene.remove(mesh));
    data.forEach((actor, i) => {
      let options = {
        index: i,
        life: 200,
      };
      addActor(actor, options).then((mesh) => {
        mesh.position.y = 1.4 - options.index / 2;
        mesh.position.x = -3.8 - options.index / 5;
        actors.push(mesh);
        scene.add(mesh);
      });
    });
  };

  updateSegment = (data) => {
    let options = {
      index: chat.length,
      life: 200,
    };
    let message = `${data.name} - ${data.details}`;
    addText(message, options).then((mesh) => {
      scene.remove(segment.shift());
      mesh.position.y = 2.3;
      mesh.scale.z = 10;
      segment.push(mesh);
      scene.add(mesh);
    });
  };

  addChat = (data) => {
    let options = {
      index: chat.length,
      life: 200,
    };
    let message = `${data.context.username}: ${data.msg}`;
    addText(message, options).then((mesh) => {
      mesh.position.x = 2;
      chat.push(mesh);
      scene.add(mesh);
    });
    console.log("Chat Recieved:", data);
  };

  // socket.on("segment", (data) => updateSegment(data));
  // socket.on("chat", (data) => addChat(data));
  // socket.on("in_booth", (data) => updateBooth(data));
  // socket.connect();

  let easeFn = BezierEasing(0.28, 0.85, 0.43, 1);

  document.title = "Vapsquad Overlay";

  // init state

  updateSegment({ msg: "VAPSQUAD" });
  addChat({ msg: "test chat!", context: { username: "dankBot" } });
  addChat({ msg: "test chat 2!", context: { username: "jason" } });

  var then = 0;
  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    render({ time }) {
      controls.update();
      if (!analyser || !vapsquadTitle) return;
      let deltaTime = time - then;
      then = time;
      // Audio Analysis

      let low = average(analyser, audioUtil.frequencies(), 50, 150);
      let mid = average(analyser, audioUtil.frequencies(), 400, 2000);
      let high = average(analyser, audioUtil.frequencies(), 2500, 15000);

      low = low > 0.2 ? (low + lastFrame.low) * 0.5 : 0.2;
      mid = mid > 0.2 ? (mid + lastFrame.mid) * 0.5 : 0.2;
      high = high > 0.2 ? (high + lastFrame.high) * 0.5 : 0.2;

      frontLight.intensity = mid + 1;
      backLight.intensity = high + 0.5;

      lastFrame.low = low;
      lastFrame.mid = mid;
      lastFrame.high = high;

      timer += 1;

      let cos = Math.cos(time);
      let sin = Math.sin(time);

      movingLight.position.x = sin * 3;

      // Update Objects State
      actors.forEach((mesh) => {
        if (mesh.life > 0) {
          mesh.rotation.x = easeFn(mesh.life / mesh.maxLife);

          mesh.life -= 8;
        }
      });

      segment.forEach((mesh) => {
        if (mesh.life > 0) {
          mesh.scale.x = Math.cos(mesh.life / 200); // position = (this * width) / 2
          mesh.rotation.x += Math.PI / 12;
          mesh.life -= 8;
        } else {
          mesh.rotation.x =
            Math.sin(time * 0.5) * Math.PI * 0.12 - Math.PI * 0.12;
        }
        mesh.scale.z = 10 + 10 * high;
      });

      for (let i = 0; i < chat.length; i++) {
        const text = chat[i];

        // chat.forEach(text => {
        let y = Math.abs(text.index - chat.length);
        // text.position.y = y - 5
        text.position.x += 0.001;
        text.position.z -= 0.005;
        text.life *= 0.995;
        text.opacity = text.life;
        text.scale.set(1, 1, 1).multiplyScalar(easeFn(text.life / 200));
        if (text.life < 0) {
          scene.remove(text);
          let sh = chat.splice(i, 1);
        }
        // if shader material
        if (text.uniforms) {
          text.material.uniforms.time.value = time;
          text.material.uniforms.high.value = high;
          text.material.uniforms.mid.value = mid;
          text.material.uniforms.low.value = low;

          if (text.index == chat.length - 1) {
          }
        }
      }

      // Update Positions and Rotation

      vapsquadTitle.scale.y = high * 10;
      vapsquadTitle.position.y = -5 + high;
      vapsquadTitle.position.z = 0;
      vapsquadTitle.position.x = -6.5;
      vapsquadTitle.material.color.r = low;
      vapsquadTitle.material.color.g = mid;
      vapsquadTitle.material.color.b = high;
      vapsquadTitle.rotation.x = Math.PI * 0.25;

      if (low / 3 > vapsquadSpaceship.phyiscs.height)
        vapsquadSpaceship.phyiscs.height = low / 3;
      let heightOffset = vapsquadSpaceship.phyiscs.height / 2;

      // TODO: somehow add physics to objects.
      let topX = 3.1;
      let topY = 1.9;
      planet.rotation.y -= 0.005; //+ mid * 0.05 + (low - 0.2) * 0.02
      planet.position.x = topX;
      planet.position.y = topY;
      planet.scale.set(1, 1, 1).multiplyScalar(low * 0.1 + 0.2);

      vapsquadSpaceship.position.x =
        sin * (vapsquadSpaceship.phyiscs.height + heightOffset) + topX;
      vapsquadSpaceship.position.z =
        cos * (vapsquadSpaceship.phyiscs.height + heightOffset); //+ topY
      vapsquadSpaceship.position.y = topY;
      vapsquadSpaceship.phyiscs.state += high - 0.2 + sin * 0.01;
      vapsquadSpaceship.rotation.z =
        Math.sin(vapsquadSpaceship.phyiscs.state) * Math.PI * 0.2;
      vapsquadSpaceship.rotation.y = time + half_pi;

      if (vapsquadSpaceship.phyiscs.height > 0.2)
        vapsquadSpaceship.phyiscs.height -= 0.001;

      // composer.render(0.1);
      renderer.render(scene, camera);
    },

    unload() {
      controls.dispose();
      renderer.dispose();
    },
  };
};

canvasSketch(sketch, settings);
